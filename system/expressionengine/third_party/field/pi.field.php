<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$plugin_info = array(
    'pi_name'        => 'Field',
    'pi_version'     => '1.0',
    'pi_author'      => 'and-ee',
    'pi_author_url'  => 'http://github.com/and-ee/field.ee2_addon',
    'pi_description' => 'Outputs field val. Optional unserialise.',
    'pi_usage'       => Field::usage()
);

class Field {

    public function __construct()
    {
        $this->EE =& get_instance();
			ee()->load->library('typography');
			ee()->typography->initialize();

    }

	public function name($session = FALSE){
		
		$data = array();
		$limit = $this->EE->TMPL->fetch_param('limit', '100');
		$name = $this->EE->TMPL->tagparts[2];
		$orderby = $this->EE->TMPL->fetch_param('orderby', 'entry_id');
		$return = NULL;
		$site_id = $this->EE->TMPL->fetch_param('site_id', '1');
		$sort = 'desc';
		$tagdata = ee()->TMPL->tagdata;
		$wrap = $this->EE->TMPL->fetch_param('wrap', 'li');
		
		if($this->EE->TMPL->fetch_param('sort') == "desc"){$sort = 'asc';}
		
		$check = $this->EE->db->get_where(
			'channel_fields',
			array(
				'field_name' => $name,
				'site_id' => $site_id
			),
			1
		);
		if ($check->num_rows() > 0)
		{
			$row = $check->row();
			$field_id = $row->field_id;
			$field_column = 'field_id_'.$row->field_id;
			$format = $row->field_fmt;
			$type = $row->field_type;
		}
		else
		{
			die("Field was not found");
		}
		  
		if ($this->EE->TMPL->fetch_param('author_id') != ''){
			$this->EE->db->where('author_id', $this->EE->TMPL->fetch_param('author_id'));
			$authcheck = $this->EE->db->get('channel_titles');
			$checkids = array();
			foreach ($authcheck->result_array() as $row)
			{
				array_push($checkids, $row['entry_id']);
			}
		}
		$this->EE->db->select('entry_id,'.$field_column);
		$this->EE->db->order_by($orderby, $sort)->limit($limit); 
		if ($this->EE->TMPL->fetch_param('entry_id') != ''){$entry_id = explode("|", $this->EE->TMPL->fetch_param('entry_id')); $this->EE->db->where_in('entry_id', $entry_id); }
		$query = $this->EE->db->get('channel_data');
		$values = array();
		foreach ($query->result_array() as $row)
		{
			$delimiter = NULL;
			if (isset($checkids)){
				if (in_array($row['entry_id'], $checkids))
				{
					if ($row[$field_column] != '')
					{
						$prefs = array(
							 'text_format'   => $format,
							 'html_format'   => 'all',
							 'auto_links'    => 'y',
							 'allow_img_url' => 'y'
						);
						$field = ee()->typography->parse_type($row[$field_column], $prefs);
						$values[$row['entry_id']] = $field;
					}
				}
			}
			else
			{
				if ($row[$field_column] != '')
				{
					$prefs = array(
						 'text_format'   => $format,
						 'html_format'   => 'all',
						 'auto_links'    => 'y',
						 'allow_img_url' => 'y'
					);
					$field = ee()->typography->parse_type($row[$field_column], $prefs);
					$values[$row['entry_id']] = $field;
				}
			}
		}
		if ($tagdata != ''){
			foreach ($values as $key => $val)
			{
				if ($type == "grid"){
					
					
					$grid_columns = $this->EE->db->get_where(
						'grid_columns',
						array(
							'field_id' => $field_id,
						)
					);
					foreach ($grid_columns->result() as $row)
					{
						$grid_column[$row->col_id] = array(
							'col_id' => (int)$row->col_id,
							'col_name' => $row->col_name,
							'col_settings' => $row->col_settings
						);
						
						
						
						$grid_data = $this->EE->db->get_where(
							'channel_grid_field_'.$field_id,
							array(
								'entry_id' => $key,
							)
						);
						foreach ($grid_data->result() as $col_data)
						{
							print_r($col_data);
						}
							$tagdata =	str_replace(
								'{'.$row->col_name.'}',
								$col_data->row_id,
								$tagdata );
							print_r($row);
						array_push($data, $tagdata);
					}
					
				}
				else {
					if ($type == "multi_select" || $type == "checkboxes"){$delimiter = "|";} 
					if (isset($delimiter)){
						$datarr = explode("|", $val);
						foreach ($datarr as $datar)
						{
							
							$replace =	str_replace(
								'{field}',
								$datar,
								$tagdata );
							array_push($data, $replace);
							
						}
					}
					else
					{
						$replace =	str_replace(
							'{field}',
							$val,
							$tagdata );
						array_push($data, $replace);
					}
				}
			}
		}	
		else 
		{
			foreach ($values as $key => $val)
			{
				if ($type == "multi_select" || $type == "checkboxes"){$delimiter = "|";} 
				if (isset($delimiter)){
					$delimited = explode("|", $val);
					foreach ($delimited as $val)
					{
						$string = '<'.$wrap.'>'.$val.'</'.$wrap.'>';
						array_push($data, $string);
					}
				}
				else
				{
					array_push($data, $val);
				}
			}
		}
		if($this->EE->TMPL->fetch_param('unique') != ''){$data = array_unique($data);}
		foreach ($data as $d)
		{
			$return .= $d;
		}
		return $return;
	}

    public static function usage()
    {
        ob_start();
    ?>


			{exp:field:name:name_of_field entry_id="" author_id="" sort="" orderby="" site_id=""}
			

    <?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

}