<?php if($message) : ?>
<h3><span class="go_notice"><?php print($message); ?></span></h3>
<?php endif; ?>
<?php if($settings_form) : ?>
<?= form_open('C=addons_extensions&M=extension_settings&file=character_limit','',array("file" => "character_limit")) ?>
<table class="mainTable" border="0" cellpadding="0" cellspacing="0">
<?php endif; ?>
<tbody>
<?php
	$out  = '';
	$name = '';
	$i	=	1;
	foreach ($field_groups->result() as $field)
	{
		if ($name != $field->group_name){
			$out .= '<tr class="header"><th>'.$field->group_name.' fields:</th><th></th><th>Field type</th><th>Limit</th><th width="110">Inherit max length</th><th width="90">Required field?</th></tr>';
		}
		$max = '';
		if (isset($field->field_maxl)){$max = '<em>('.$field->field_maxl.')</em>';}
		$req = 'No';
		if ($field->field_required == 'y'){$req = '<em>Yes</em>';}
		$out .= '<tr class="'.(($i&1) ? "odd" : "even").'">
			<td><strong>'.$field->field_label.'</strong></td><td><input style="background:none;border:none;" readonly value="{'.$field->field_name.'}"></td>
			<td width="100">'.$field->field_type.'</td>
			<td><input dir="ltr" style="width: 100%;" name="'.$input_prefix.'['.$field->field_id.'][limit]" value="'.((isset($settings[$field->field_id]['limit'])) ? $settings[$field->field_id]['limit'] : '' ).'" size="20" maxlength="120" class="input" type="number"></td>
			<td><input  name="'.$input_prefix.'['.$field->field_id.'][max]" value="true" '.((isset($settings[$field->field_id]['max'])) ? 'checked' : '' ).' type="checkbox"> '.$max.'</td>
			<td>'.$req.'</td>
		</tr>';
		$name = $field->group_name;
		$i++;
	}	
	echo $out;
?>	
</tbody>		
</table>
<p><input name="edit_field_group_name" value="<?= lang('save_extension_settings'); ?>" class="submit" type="submit"></p>
<?= form_close(); ?>





