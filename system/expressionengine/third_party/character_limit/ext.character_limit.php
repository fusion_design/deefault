<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}

class Character_limit_ext
{
	var $settings = array();
	var $addon_name = 'Character Limit';
	var $name = 'Character Limit';
	var $version = '1.0';
	var $description = '';
	var $settings_exist = 'y';
	var $docs_url = '';
	private $hooks = array('cp_js_end' => 'cp_js_end');

	function Character_limit_ext($settings = '')
	{
		$this->EE =& get_instance();
		$this->settings = $settings;
	}

	public function __construct($settings = FALSE)
	{
		$this->EE =& get_instance();
		if (defined('SITE_ID') == FALSE) {
			define('SITE_ID', $this->EE->config->item('site_id'));
		}
		$this->settings = ($settings == FALSE) ? $this->_getSettings() : $this->_saveSettingsToSession($settings);
	}

	public function settings_form()
	{
		$this->EE->lang->loadfile('character_limit');
		ee()->load->model('channel_model'); 
		$vars = array(
			'addon_name' => $this->addon_name,
			'error' => FALSE,
			'input_prefix' => __CLASS__,
			'message' => FALSE,
			'settings_form' => FALSE,
			'channel_data' => ee()->channel_model->get_channels()->result(),
			'field_groups' => ''
		);
		$vars['settings']      = $this->settings;
		$vars['settings_form'] = TRUE;
		if ($new_settings = $this->EE->input->post(__CLASS__)) {
			$vars['settings'] = $new_settings;
			$this->_saveSettings($new_settings);
			$vars['message'] = $this->EE->lang->line('extension_settings_saved_success');
		}
		$vars['field_groups']    = $this->field_groups();
		return $this->EE->load->view('form_settings', $vars, true);
	}

	function cp_js_end() 
	{
		$out = $this->EE->extensions->last_call;
		$this->EE->load->library('javascript');
		$data = $this->settings;
		foreach ($data as $id => $arr) {
			if(isset($arr['max']) || $arr['limit'] > 1){
				
				if (isset($arr['max'])){$limit = $arr['max'];}
				else{$limit = $arr['limit'];}
				$out .= '
$(function(){
	$("[name=field_id_'.$id.']").parent().append("<div class=\'field_count\' style=\'background:#27343C;color:#fff;float:right;display:block;padding:0 5px;line-height:20px;\' id=\'field_count_'.$id.'\'></div>");
	$("[name=field_id_'.$id.']").on("keyup mouseup", function(){
		characters = $(this).val().length;
		limit = "'.$limit.'";
		if (limit == "true"){limit = $(this).attr("maxlength");}
		$("#field_count_'.$id.'").text(characters+"/"+limit);
		if(characters > limit){
			$("#field_count_'.$id.'").css("background", "#d91350");
		}
		else{
			$("#field_count_'.$id.'").css("background", "#27343C");
		}
	});
});' . "\r\n";
			}
		}
		return $out;
	}

	function field_groups()
	{
		$out = $this->EE->db->query("SELECT cf.field_id, cf.group_id, cf.field_order, cf.field_type, cf.field_required, cf.field_maxl, fg.group_name, cf.field_label, cf.field_name
			FROM exp_channel_fields cf
			JOIN exp_field_groups fg ON fg.group_id = cf.group_id 
			WHERE cf.site_id = " . SITE_ID . "
			ORDER BY cf.group_id ASC, cf.field_order ASC");
		return $out;
	}
	
	function activate_extension()
	{
		$this->_createHooks();
	}

	private function _getSettings($refresh = FALSE)
	{
		$settings = FALSE;
		if (isset($this->EE->session->cache[$this->addon_name][__CLASS__]['settings']) === FALSE || $refresh === TRUE) {
			$settings_query = $this->EE->db->select('settings')->where('enabled', 'y')->where('class', __CLASS__)->get('extensions', 1);
			if ($settings_query->num_rows()) {
				$settings = unserialize($settings_query->row()->settings);
				$this->_saveSettingsToSession($settings);
			}
		} 
		else {
			$settings = $this->EE->session->cache[$this->addon_name][__CLASS__]['settings'];
		}
		return $settings;
	}

	private function _saveSettingsToSession($settings, &$sess = FALSE)
	{
		if ($sess == FALSE && isset($this->EE->session->cache) == FALSE){
			return $settings;
		}
		if ($sess == FALSE && isset($this->EE->session) == TRUE){
			$sess =& $this->EE->session;
		}
		$sess->cache[$this->addon_name][__CLASS__]['settings'] = $settings;
		return $settings;
	}

	private function _saveSettings($settings)
	{
		$this->EE->db->where('class', __CLASS__)->update('extensions', array('settings' => serialize($settings)));
	}
	
	private function _createHooks($hooks = FALSE)
	{
		if (!$hooks) {
			$hooks = $this->hooks;
		}
		$hook_template = array(
			'class' => __CLASS__,
			'settings' => '',
			'priority' => '1',
			'version' => $this->version
		);
		$hook_template['settings']['multilanguage'] = 'n';
		foreach ($hooks as $key => $hook) {
			if (is_array($hook)) {
				$data['hook'] = $key;
				$data['method'] = (isset($hook['method']) === TRUE) ? $hook['method'] : $key;
				$data = array_merge($data, $hook);
			} else {
				$data['hook'] = $data['method'] = $hook;
			}
			$hook = array_merge($hook_template, $data);
			$hook['settings'] = serialize($hook['settings']);
			$this->EE->db->query($this->EE->db->insert_string('exp_extensions', $hook));
		}
	}

	private function _deleteHooks()
	{
		$this->EE->db->query("DELETE FROM `exp_extensions` WHERE `class` = '" . __CLASS__ . "'");
	}
	
	function disable_extension()
	{
		$this->EE->db->delete('exp_extensions', array('class' => get_class($this)));
	}
}
