<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prepend_short_name_acc {

	var $name		= 'Prepend short name';
	var $id			= 'prepend_short_name';
	var $version		= '1.1';
	var $description	= "Prepends the auto-generated custom field short name with the channel's short name";
	var $sections		= array();

	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->EE =& get_instance();
	}
	
	/**
	 * Set Sections
	 *
	 * Set content for the accessory
	 *
	 * @access	public
	 * @return	void
	 */
	private function _js($name) {
		return '<input type="hidden" id="fieldurlset" value="cat"><script>$("#field_label").live("keydown keyup blur",function(){title = $("#field_label").val();$("#fieldurlset").val(title);$("#fieldurlset").ee_url_title($("#field_name")); v = $("#field_name").val(); $("#field_name").val("'.$name.'-"+v); });</script>';
	}
	function set_sections() {
		$this->sections[] = '<script type="text/javascript">$("#accessoryTabs a.' . $this->id . '").parent().remove();</script>';
		if(isset($_GET['C'])){
			if($_GET['C'] == 'admin_content' && $_GET['M'] == 'field_edit') {
				if(isset($_GET['group_id'])){
					if(!isset($_GET['field_id'])){
						$group_id = $_GET['group_id'];				
						$sql = "SELECT channel_name, channel_id FROM exp_channels WHERE field_group = $group_id";
						$query = $this->EE->db->query($sql);
						if ($query->num_rows() == 1){
							foreach($query->result_array() as $row) {
								$this->EE->cp->add_to_foot($this->_js($row['channel_name']));
							}
						}
						else if ($query->num_rows() > 1){
							foreach($query->result_array() as $row) {
								$id = $row['channel_id'];
								if ($group_id == $id) {
									$this->EE->cp->add_to_foot($this->_js($row['channel_name']));
								}
							}
						}
						else {
							$this->EE->cp->add_to_foot('<script>$("#mainContent form").prepend("<div class=\'ui-state-error\' style=\'padding:10px;\'><strong>This field group is currently unassigned to a channel.</strong></div>");</script>');
						}
					}
				}
			}
		}
	}
	
}
// END CLASS