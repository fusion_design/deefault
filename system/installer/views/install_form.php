<?php
if ($errors != '')
{
	echo '<div class="well">';
	echo '<h2 class="important">'.lang('error').'</h2>';
	echo '<h5 class="important">'.lang('error_occurred').'</h5>';
	echo $errors;
	echo '</div>';
}
else
{
	echo '<h2>'.lang('enter_settings').'</h2>';
	echo '<p class="important">'.lang('contact_host').'</p>';
}
?>
<div class="modal fade" id="message">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title">Domain details</h4>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label class="control-label" for="acc_name">Account name</label>
				<input autocomplete="off" type='text' name='acc_name' id="acc_name" class="form-control" value="" placeholder="fusion">
			</div>
			<div class="form-group">
				<label class="control-label" for="acc_sitename">Site name</label>
				<input autocomplete="off" type='text' name='acc_sitename' id="acc_sitename" class="form-control" value="" placeholder="Fusion Design">
			</div>
			<div class="form-group">
				<label class="control-label" for="acc_url">Site url (not the temporary address, no http or www)</label>
				<input autocomplete="off" type='text' name='acc_url' id="acc_url" class="form-control" value="" placeholder="fusionweb.co.uk">
			</div>
			<div class="form-group">
				<label class="control-label" for="acc_pass">Site password</label>
				<input autocomplete="off" type='text' name='acc_pass' id="acc_pass" class="form-control" value="" placeholder="">
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="confirm" class="btn btn-success">Confirm details</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link rel="stylesheet" href="http://cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css">
<form method='post' action='<?=$action?>' id="installForm" autocomplete="off" role="form">
<style>
body{background:#27343c !important;}
.autofil_capture {position: fixed;width: 1px;left: -50px;}
.autofil_capture input {width: 1%;}
.hide{display:none;}
h3{margin-top:5px;}
</style>
<div class="autofil_capture">
    <input class="js-clear_field" tabindex="-1" name="blank_username" type="email">
    <input class="js-clear_field" tabindex="-1" name="blank_password" type="password">
    <input class="js-clear_field" tabindex="-1" name="blank_email" type="email">
</div>
<?php if (IS_CORE): ?>
	<input autocomplete="off" type="hidden" value="<?=$license_number?>" />
<?php else: ?>
<div class="well hide">
	<div class="form-group">
		<label class="control-label" for="license_number"><?=lang('license_number')?></label>
		<input autocomplete="off" type='text' name='license_number' id="license_number" class="form-control" value="<?=$license_number?>" placeholder="Enter license number" title="<?=lang('locate_license_number')?>">
	</div>
	<small><?=lang('locate_license_number')?></small>
</div>
<?php endif; ?>
<div class="well">
	<h3 class="text-primary"><?=lang('server_settings')?></h3>
	<div class="form-group">
		<label class="control-label" for="site_index"><?=lang('name_of_index')?></label>
		<input autocomplete="off" type='text' name='site_index' id="site_index" class="form-control" value="" placeholder="" title="<?=lang('normally_index')?>">
	</div>
	<div class="form-group">
		<label class="control-label" for="site_url"><?=lang('url_of_index')?></label>
		<input autocomplete="off" type='text' name='site_url' id="site_url" class="form-control" value="<?=$site_url?>" title="<?=lang('normally_root')?>">
	</div>
	<div class="form-group">
		<label class="control-label" for="cp_url"><?=lang('url_of_admin')?></label>
		<input autocomplete="off" type='text' name='cp_url' id="cp_url" class="form-control" value="<?=$cp_url?>" title="<?=lang('url_of_admin_info')?>">
	</div>
	<div class="form-group">
		<label class="control-label" for="webmaster_email"><?=lang('webmaster_email')?></label>
		<input autocomplete="off" type='email' name='webmaster_email' id="webmaster_email" class="form-control" value="info@fusionweb.co.uk">
	</div>
</div>
<div class="well">
	<h3 class="text-primary"><?=lang('database_settings')?></h3>
	<?php if (count($databases) == 1):?>
		<?=form_hidden('dbdriver', key($databases))?>
	<?php else:?>
		<div class="form-group">
			<label class="control-label" for="dbdriver"><?=lang('database_type')?></label>
			<?=form_dropdown('dbdriver', $databases, $dbdriver, 'class="select form-control" id="dbdriver"')?>
		</div>
	<?php endif;?>
	<div class="form-group" >
		<label class="control-label" for="db_hostname"><?=lang('sql_server_address')?></label>
		<input autocomplete="off" type='text' name='db_hostname' id="db_hostname" class="form-control" value="<?=$db_hostname?>" placeholder="" title="<?=lang('usually_localhost')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="db_name"><?=lang('sql_dbname')?></label>
		<input autocomplete="off" type='text' name='db_name' id="db_name" class="form-control" value="site_db" placeholder="" title="<?=lang('sql_dbname_info')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="db_user"><?=lang('sql_username')?></label>
		<input autocomplete="off" type='text' name='db_username' id="db_user" class="form-control" value="site_user" placeholder="" title="<?=lang('sql_username_info')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="db_password"><?=lang('sql_password')?></label>
		<input autocomplete="off" type='text' name='db_password' id="db_password" class="form-control" value="pass" placeholder="" title="<?=lang('sql_password_info')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="db_prefix"><?=lang('sql_prefix')?></label>
		<input autocomplete="off" type='text' name='db_prefix' id="db_prefix" class="form-control" value="<?=$db_prefix?>" placeholder="" title="<?=lang('sql_prefix_info')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="db_conntype_nonpersistent"><?=lang('sql_conntype')?></label>
		<div class="radio">
			<label for="db_conntype_nonpersistent"><input autocomplete="off" type="radio" class='radio' name="db_conntype" value="nonpersistent" id="db_conntype_nonpersistent" <?=$nonpersistent?> /> <?=lang('nonpersistent')?></label>
		</div>
		<div class="radio">
			<label for="db_conntype_persistent"><input autocomplete="off" type="radio" class='radio' name="db_conntype" value="persistent" id="db_conntype_persistent" <?=$persistent?> /> <?=lang('persistent')?></label>
		</div>
	</div>
</div>
<div class="well">
	<h3 class="text-primary"><?=lang('create_account')?></h3>
	<div class="form-group" >
		<label class="control-label" for="username"><?=lang('username')?></label>
		<input autocomplete="off" type='text' name='username' id="username" class="form-control" value="info@fusionweb.co.uk" placeholder="" title="<?=lang('4_chars')?>">
	</div>
<script type="text/javascript" charset="utf-8">
	function confirm_password()
	{
		if (document.getElementById('password_confirm').value != document.getElementById('password').value)
		{
			str = '<p class="important"><?=lang('password_mismatch')?></p>';
		}
		else
		{
			str = '<p class="success"><?=lang('password_confirmed')?></p>';
		}

		document.getElementById('password_mismatch').innerHTML = str;
	}
</script>
	<div class="form-group" >
		<label class="control-label" for="password"><?=lang('password')?></label>
		<input autocomplete="off" type='text' name='password' id="password" class="form-control" value="<?=$password?>" placeholder="" title="<?=lang('5_chars')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="password_confirm"><?=lang('password_confirm')?></label>
		<input autocomplete="off" type='text' name='password_confirm' id="password_confirm" class="form-control" value="<?=$password_confirm?>" placeholder="" title="<?=lang('pw_retype')?>" onkeyup="confirm_password();return false;">
	</div>
	<div id="password_mismatch" class="pad"></div>
	<div class="form-group" >
		<label class="control-label" for="email_address"><?=lang('email')?></label>
		<input autocomplete="off" type='email' name='email_address' id="email_address" class="form-control" value="info@fusionweb.co.uk" placeholder="">
	</div>
	<div class="form-group" >
		<label class="control-label" for="screen_name"><?=lang('screen_name')?></label>
		<input autocomplete="off" type='text' name='screen_name' id="screen_name" class="form-control" value="Fusion Design" placeholder="" title="<?=lang('screen_name_info')?>">
	</div>
	<div class="form-group" >
		<label class="control-label" for="site_label"><?=lang('site_label')?></label>
		<input autocomplete="off" type='text' name='site_label' id="site_label" class="form-control" value="<?=$site_label?>" placeholder="">
	</div>
</div>
<div class="well">
	<h3 class="text-primary"><?=lang('deft_template')?></h3>
	<div class="form-group" >
		<label class="control-label" for="theme_select">Choose to start with a blank or pre-populated site</label>
		<select name='theme' class='form-control' id="theme_select">
			<option value=''>None - Empty Installation</option>
		<?php
		
			foreach ($themes as $key => $val)
			{
				$selected = ($theme == $key) ? " selected" : "";
				?><option value='<?=$key?>'<?=$selected?>><?=$val?></option><?php echo "\n";
			}
		?>
	
		</select>
	</div>
</div>
<div class="well ">
	<h3 class="text-danger"><?=lang('optional_modules')?></h3>
<p><?=lang('optional_modules_info')?></p>
<table>
<tr>
<?php unset($modules['rte'], $modules['ip_to_nation']) ?>
<?php $i = 0; foreach ($modules as $key => $name): ?>
<?php if ($i++ % 3 == 0):?></tr><tr><?php endif; ?>
<?php $checked = ($name['checked'] === TRUE) ? "checked='checked'" : ''; ?>
<td><input autocomplete="off" type='checkbox' name='modules[]' value='<?=$key?>' id='<?=$key?>' <?=$checked?>/> <label for='<?=$key?>'><?=$name['name']?><span class="req_module"> *</span></label></td>
<?php endforeach; ?>
</tr>
</table>
<p><?=lang('template_required_modules')?></p>

</div>



<div class="well hide">
	<h3 class="text-primary"><?=lang('local_settings')?></h3>
	
	<div class="form-group" >
		<label class="control-label" for="timezone_select"><?=lang('timezone')?></label>
		<p><?=$this->localize->timezone_menu($default_site_timezone, 'default_site_timezone')?></p>
	</div>

</div>





<p class="clearfix"><?php echo form_submit('', lang('install_ee'), 'class="submit btn btn-primary pull-right"'); ?></p>

<script type="text/javascript" charset="utf-8" src="http://cdn.jsdelivr.net/bootstrap/latest/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
$(function(){
	$('input[title]').tooltip({html : true, placement:"top"});
	$('#message').modal({
	  keyboard: false,
	  backdrop: 'static'
	});
	$(document).on('click', '#confirm', function(){
		acc_url = $('#acc_url').val();
		acc_name = $('#acc_name').val();
		acc_sitename = $('#acc_sitename').val();
		acc_pass = $('#acc_pass').val();
		$('#webmaster_email').val('info@'+acc_url);
		$('#db_name').val(acc_name+'_db');
		$('#db_user').val(acc_name+'_user');
		$('#db_password').val(acc_pass);
		$('#password').val(acc_pass);
		$('#password_confirm').val(acc_pass);
		$('#site_label').val(acc_sitename);
		$('#message').modal('hide');
	})
});
</script>

<?php echo form_close();
/* End of file install_form.php */
/* Location: ./system/expressionengine/installer/views/install_form.php */